import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ImageService {

  private paath:string = 'https://firebasestorage.googleapis.com/v0/b/hello-ami.appspot.com/o/';
  public images:string[] = [];
  constructor() {
    this.images[0] = this.paath + 'biz.jpeg' + '?alt=media&token=9e7581ae-2bc2-439c-ac1f-c719628b24f8';
    this.images[1] = this.paath + 'entrmnt.jpeg' + '?alt=media&token=2e3c1859-44ae-4652-af4a-9a9e73886acf';
    this.images[2] = this.paath + 'politics-icon.jpeg' + '?alt=media&token=ab5f023e-23cf-40b3-916f-f5ec1a5c8098';
    this.images[3] = this.paath + 'sport.jpeg' + '?alt=media&token=e0683705-dd69-411a-8798-b61f5e2e9fd0';
    this.images[4] = this.paath + 'tech.jpeg' + '?alt=media&token=e37f3a65-d824-41c7-9dc5-7b993bc9993d';
   }
}
